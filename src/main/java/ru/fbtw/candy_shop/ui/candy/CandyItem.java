package ru.fbtw.candy_shop.ui.candy;

import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Rectangle;
import ru.fbtw.candy_shop.core.Candy;
import ru.fbtw.candy_shop.ui.UiUtils;
import ru.fbtw.candy_shop.ui.Widget;


public class CandyItem implements Widget {
	public static final double WIDTH = 300;
	public static final double HEIGHT = 300;

	private final Candy candy;
	private final VBox mainLayout;
	private final StackPane container;
	private final Rectangle shadowCaster;
	private final CandyModel model;


	public CandyItem(Candy candy) {
		this.candy = candy;

		mainLayout = new VBox();
		mainLayout.getStyleClass().add("item-layout");
		model = CandyModel.getRandomModel(100);

		mainLayout.getChildren().addAll(
				model.getView(),
				buildLabel("name", candy.getName()),
				buildLabel("price", Integer.toString(candy.getPrice()))
		);

		shadowCaster = new Rectangle();
		shadowCaster.getStyleClass().add("shadow-caster");

		container = new StackPane();
		container.getChildren().addAll(shadowCaster,mainLayout);

		resize(WIDTH,HEIGHT);
	}

	private Label buildLabel(String styleClassName, String text) {
		Label label = new Label(text);
		label.getStyleClass().add(styleClassName);
		return label;
	}

	@Override
	public void resize(double width, double height) {
		UiUtils.setFixedSize(container, width, height);
		UiUtils.setFixedSize(mainLayout, width, height);

		shadowCaster.setWidth(width);
		shadowCaster.setHeight(height);

		model.setWidth(width);
	}

	@Override
	public Node getView() {
		return container;
	}

	public Candy getCandy() {
		return candy;
	}
}
