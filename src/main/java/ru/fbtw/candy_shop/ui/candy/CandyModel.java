package ru.fbtw.candy_shop.ui.candy;

import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Polygon;
import ru.fbtw.candy_shop.ui.Widget;

public class CandyModel implements Widget {

	/**
	 * Соотношение сторон
	 */
	private static final double ASPECT_RATIO = 3.0 / 4.0;

	/**
	 * Соотношение относительного положения точки треугольника(ближайшая к центру круга) к радиусу круга
	 */
	private static final double DELTA_P_RATIO = 2.0 / 10.0;

	/**
	 * Доля от {@link #width}, которая выражает расстояние от краев {@link #container} до точек треугольников
	 */
	private static final double DELTA_L_RATIO = 1.0 / 10.0;

	private double width;
	private double height;

	private final Circle main;
	private final Polygon leftTriangle;
	private final Polygon rightTriangle;

	private CandyDecoration decoration;

	private final Pane container;

	public CandyModel(double width, CandyDecoration decoration) {
		this.width = width;
		this.decoration = decoration;

		main = new Circle();
		leftTriangle = new Polygon();
		rightTriangle = new Polygon();

		container = new Pane();
		container.getChildren().addAll(leftTriangle, rightTriangle, main);
		VBox.setVgrow(container, Priority.ALWAYS);

		updateSize();
		applyDecoration();
	}

	private void updateSize() {
		height = width * ASPECT_RATIO;

		container.setMinSize(width, height);
		container.setMinSize(width, height);
		container.setMaxSize(width, height);

		double r = width / 4;

		main.setRadius(r);
		main.setCenterX(width / 2);
		main.setCenterY(height / 2);

		setupPoints(leftTriangle.getPoints(), r, true);
		setupPoints(rightTriangle.getPoints(), r, false);
	}

	private void setupPoints(ObservableList<Double> points, double r, boolean direction) {
		double dp = r * DELTA_P_RATIO;
		double dl = width * DELTA_L_RATIO;

		Double[] newPoints = {
				Math.abs((direction ? 0 : width) - dl), height / 2 - r,
				Math.abs((direction ? 0 : width) - dl), height / 2 + r,
				width / 2 + dp * (direction ? -1 : 1), height / 2
		};

		points.clear();
		points.addAll(newPoints);
	}

	private void applyDecoration() {
		main.setFill(decoration.getMainColor());

		leftTriangle.setFill(decoration.getAdditionalColor());
		rightTriangle.setFill(decoration.getAdditionalColor());
	}

	public void setWidth(double width) {
		this.width = width;
		updateSize();
	}

	public void setDecoration(CandyDecoration decoration) {
		this.decoration = decoration;
		applyDecoration();
	}

	public double getWidth() {
		return width;
	}

	public double getHeight() {
		return height;
	}

	public CandyDecoration getDecoration() {
		return decoration;
	}

	@Override
	public void resize(double width, double height) {
		setWidth(width);
	}

	@Override
	public Node getView() {
		return container;
	}

	public static CandyModel getRandomModel(double width) {
		CandyDecoration decoration = CandyDecoration.getRandomDecoration();
		return new CandyModel(width, decoration);
	}
}
