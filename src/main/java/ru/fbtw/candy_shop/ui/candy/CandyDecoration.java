package ru.fbtw.candy_shop.ui.candy;

import javafx.scene.paint.Color;

import java.util.Random;

public class CandyDecoration {
	private Color mainColor;
	private Color additionalColor;

	public CandyDecoration(Color mainColor, Color additionalColor) {
		this.mainColor = mainColor;
		this.additionalColor = additionalColor;
	}

	public Color getMainColor() {
		return mainColor;
	}

	public void setMainColor(Color mainColor) {
		this.mainColor = mainColor;
	}

	public Color getAdditionalColor() {
		return additionalColor;
	}

	public void setAdditionalColor(Color additionalColor) {
		this.additionalColor = additionalColor;
	}

	public static CandyDecoration getRandomDecoration() {
		return new CandyDecoration(nextColor(),nextColor());
	}

	private static Color nextColor() {
		Random random = new Random();

		int color = random.nextInt(0x1000000);
		StringBuilder tmp = new StringBuilder(Integer.toHexString(color));

		while (tmp.length() < 6){
			tmp.insert(0, "0");
		}

		String webColor = String.format("#%s", tmp.toString());
		return Color.web(webColor);
	}
}
