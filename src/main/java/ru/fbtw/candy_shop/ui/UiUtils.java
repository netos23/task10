package ru.fbtw.candy_shop.ui;

import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.Region;
import javafx.stage.Stage;

public class UiUtils {
	public static void setFixedSize(Region region, double width, double height){
		setFixedHeight(region,height);
		setFixedWidth(region, width);
	}
	public static void setFixedWidth(Region region, double width){
		region.setMinWidth(width);
		region.setPrefWidth(width);
		region.setMaxWidth(width);
	}
	public static void setFixedHeight(Region region, double height){
		region.setMinHeight(height);
		region.setPrefHeight(height);
		region.setMaxHeight(height);
	}

	public static Stage getStage(Node node){
		return (Stage) node.getScene().getWindow();
	}

	public static Label buildTitle(String name, String style) {
		Label label = new Label(name);
		label.getStyleClass().add(style);
		return label;
	}
}
