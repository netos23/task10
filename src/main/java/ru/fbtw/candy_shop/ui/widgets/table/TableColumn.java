package ru.fbtw.candy_shop.ui.widgets.table;

import javafx.scene.Node;

import java.util.Comparator;

public class TableColumn<T, V extends Node> {
	private String name;
	private Comparator<T> comparator;
	private ColumnSpliterator<T, V> spliterator;

	public TableColumn(String name, Comparator<T> comparator, ColumnSpliterator<T, V> spliterator) {
		this.name = name;
		this.comparator = comparator;
		this.spliterator = spliterator;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Comparator<T> getComparator() {
		return comparator;
	}

	public void setComparator(Comparator<T> comparator) {
		this.comparator = comparator;
	}

	public ColumnSpliterator<T, V> getSpliterator() {
		return spliterator;
	}

	public void setSpliterator(ColumnSpliterator<T, V> spliterator) {
		this.spliterator = spliterator;
	}


}
