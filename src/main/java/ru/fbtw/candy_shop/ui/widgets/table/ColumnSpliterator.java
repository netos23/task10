package ru.fbtw.candy_shop.ui.widgets.table;

import javafx.scene.Node;

@FunctionalInterface
public interface ColumnSpliterator<T, V extends Node> {
	V split(T source);
}
