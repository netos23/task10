package ru.fbtw.candy_shop.ui.widgets.table;

import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import ru.fbtw.candy_shop.ui.Widget;

import java.util.*;

public class Table<T, V extends Node> implements Widget {

	private GridPane mainLayout;
	private List<TableColumn<T, V>> columns;
	private List<T> data;
	private List<Button> headers;

	private String sortIndex;
	private boolean isReversed;

	public Table(List<TableColumn<T, V>> columns) {
		this.columns = columns;
		data = new ArrayList<>();
		mainLayout = new GridPane();
		mainLayout.getStyleClass().add("table");
		headers = new ArrayList<>();
		buildHeaders();
	}

	private void buildHeaders() {
		columns.stream()
				.map(TableColumn::getName)
				.map(Button::new)
				.peek(button -> button.getStyleClass().clear())
				.peek(this::setSortClick)
				.peek(label -> label.getStyleClass().add("table-header"))
				.forEach(headers::add);
	}

	public void addData(List<T> data) {
		this.data.addAll(data);
		updateTable();
	}

	public void addData(T... data) {
		addData(Arrays.asList(data));
	}

	private void setSortClick(Button button) {
		button.setOnAction(event -> {
			if (button.getText().equals(sortIndex)) {
				isReversed = !isReversed;
				Collections.reverse(data);
				updateTable();
			}else{
				sortIndex = button.getText();
				Optional<TableColumn<T, V>> column = columns.stream()
						.filter(v -> v.getName().equals(sortIndex))
						.findAny();
				isReversed = false;
				column.ifPresent(tc -> data.sort(tc.getComparator()));
				updateTable();
			}

		});
	}


	private void updateTable() {
		mainLayout.getChildren().clear();
		setHeaders();
		int colCount = columns.size();
		for (int i = 0; i < data.size(); i++) {
			for (int j = 0; j < colCount; j++) {
				Node node = columns.get(j)
						.getSpliterator()
						.split(data.get(i));
				node.getStyleClass().add("custom-table-cell");

				mainLayout.add(node, j, 1 + i % colCount);
			}
		}
	}

	private void setHeaders() {
		for (int i = 0; i < headers.size(); i++) {
			mainLayout.add(headers.get(i), i, 0);
		}
	}


	@Override
	public void resize(double width, double height) {

	}

	@Override
	public Parent getView() {
		return mainLayout;
	}
}
