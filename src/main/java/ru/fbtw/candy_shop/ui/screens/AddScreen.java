package ru.fbtw.candy_shop.ui.screens;


import javafx.event.ActionEvent;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import ru.fbtw.candy_shop.core.Candy;
import ru.fbtw.candy_shop.ui.UiUtils;
import ru.fbtw.candy_shop.ui.Widget;
import ru.fbtw.candy_shop.ui.candy.CandyItem;

import java.util.InputMismatchException;
import java.util.function.Consumer;

import static ru.fbtw.candy_shop.ui.UiUtils.buildTitle;

public class AddScreen implements Widget {
	private static final String NAME = "Candy name";
	private static final String PRICE = "Price";
	private static final String ERROR_MSG = "Unexpected price, please use only integer digits";
	private static final String ERROR_EMPTY = "One or more fields were left blank";

	private TextField nameInput, priceInput;
	private Button submit;
	private Label err;

	private VBox layout;

	private Consumer<CandyItem> itemConsumer;

	public AddScreen() {
		layout = new VBox();
		layout.getStyleClass().add("form-layout");

		Label name = buildTitle(NAME, "input-title");
		Label price = buildTitle(PRICE, "input-title");
		err = buildTitle(ERROR_MSG, "err");
		err.setVisible(false);

		nameInput = new TextField();
		priceInput = new TextField();

		submit = new Button("submit");
		submit.setOnAction(this::submitCandy);

		layout.getChildren()
				.addAll(name, nameInput, price, priceInput, submit, err);

		itemConsumer = (v) -> {
		};
	}



	public void setItemConsumer(Consumer<CandyItem> consumer) {
		this.itemConsumer = consumer;
	}

	private void submitCandy(ActionEvent event) {
		try {
			String name = nameInput.getText();
			if(name == null || name.isEmpty()){
				throw new InputMismatchException();
			}
			String priceSrc = priceInput.getText();
			int price = Integer.parseInt(priceSrc);

			Candy candy = new Candy(name, price);
			CandyItem item = new CandyItem(candy);
			itemConsumer.accept(item);

			UiUtils.getStage(layout).close();
		} catch (NumberFormatException ex) {
			setErr(ERROR_MSG);
		} catch (InputMismatchException ex){
			setErr(ERROR_EMPTY);
		}
	}

	private void setErr(String errorMsg) {
		err.setText(errorMsg);
		err.setVisible(true);
	}

	@Override
	public void resize(double width, double height) {
		// fixed-size widget
	}

	@Override
	public Parent getView() {
		return layout;
	}
}
