package ru.fbtw.candy_shop.ui.screens;

import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.VBox;
import ru.fbtw.candy_shop.core.Candy;
import ru.fbtw.candy_shop.core.CandyPackage;
import ru.fbtw.candy_shop.ui.Widget;
import ru.fbtw.candy_shop.ui.candy.CandyModel;
import ru.fbtw.candy_shop.ui.widgets.table.Table;
import ru.fbtw.candy_shop.ui.widgets.table.TableColumn;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static ru.fbtw.candy_shop.ui.UiUtils.buildTitle;

public class CartScreen implements Widget {

	private static final String BALANCE_AMOUNT = "Account balance: %d";
	private final CandyPackage candyPackage;
	private ScrollPane mainLayout;
	private Table<TableCandyItem, Node> tableView;

	public CartScreen(CandyPackage candyPackage, Map<Candy, CandyModel> modelMap) {
		this.candyPackage = candyPackage;
		tableView = new Table<>(getColumns());
		tableView.addData(toTableItems(candyPackage, modelMap));

		VBox layout = new VBox();
		layout.getStyleClass().add("cart-layout");
		Label balance = buildTitle(String.format(BALANCE_AMOUNT, candyPackage.getBalance()), "price");
		layout.getChildren().addAll(tableView.getView(), balance);
		mainLayout = new ScrollPane(layout);

	}

	private List<TableColumn<TableCandyItem, Node>> getColumns() {
		return new ArrayList<TableColumn<CartScreen.TableCandyItem, Node>>() {{
			add(new TableColumn<>("Image", CartScreen.this::sortNames, CartScreen.TableCandyItem::getLabelModel));
			add(new TableColumn<>("Name", CartScreen.this::sortNames, CartScreen.TableCandyItem::getLabelName));
			add(new TableColumn<>("Price", CartScreen.this::sortPrices, CartScreen.TableCandyItem::getLabelPrice));
			add(new TableColumn<>("Count", CartScreen.this::sortCount, CartScreen.TableCandyItem::getLabelCount));
		}};
	}

	private int sortCount(TableCandyItem o1, TableCandyItem o2) {
		return Integer.compare(o1.getCount(), o2.getCount());
	}

	private int sortPrices(TableCandyItem o1, TableCandyItem o2) {
		return Integer.compare(o1.getCandy().getPrice(), o2.getCandy().getPrice());
	}

	private int sortNames(TableCandyItem o1, TableCandyItem o2) {
		return o1.getCandy()
				.getName()
				.compareTo(o2.getCandy().getName());
	}


	private List<TableCandyItem> toTableItems(CandyPackage candyPackage, Map<Candy, CandyModel> modelMap) {
		List<TableCandyItem> list = new ArrayList<>();
		Map<Candy, Integer> cart = candyPackage.getCart();
		for (Candy candy : cart.keySet()) {
			TableCandyItem item = new TableCandyItem(candy, modelMap.get(candy), cart.get(candy));
			list.add(item);
		}
		return list;
	}

	@Override
	public void resize(double width, double height) {

	}

	@Override
	public Parent getView() {
		return mainLayout;
	}


	private class TableCandyItem {
		private final Candy candy;
		private final CandyModel model;
		private final int count;

		private Label name;
		private Label price;
		private Label countLabel;

		public TableCandyItem(Candy candy, CandyModel model, int count) {
			this.candy = candy;
			this.model = model;
			this.count = count;
		}

		public Label getLabelName() {
			if (name == null) {
				name = buildTitle(candy.getName(), "table-item");
			}
			return name;
		}

		public Label getLabelPrice() {
			if (price == null) {
				price = buildTitle(Integer.toString(candy.getPrice()), "table-item");
			}
			return price;
		}

		public Node getLabelModel() {
			return model.getView();
		}

		public Label getLabelCount() {
			if (countLabel == null) {
				countLabel = buildTitle(Integer.toString(count), "table-item");
			}
			return countLabel;
		}

		public Candy getCandy() {
			return candy;
		}

		public CandyModel getModel() {
			return model;
		}

		public int getCount() {
			return count;
		}
	}
}

