package ru.fbtw.candy_shop.ui.screens;

import javafx.event.ActionEvent;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import ru.fbtw.candy_shop.ui.UiUtils;
import ru.fbtw.candy_shop.ui.Widget;

import java.util.Optional;
import java.util.function.Consumer;

public class BalanceScreen implements Widget {
	private Consumer<Integer> consumer;
	private int balance;
	private TextField input;
	private Button submit;
	private VBox mainLayout;
	private Label error;

	public BalanceScreen(Consumer<Integer> consumer, int balance) {
		this.consumer = consumer;
		this.balance = balance;

		mainLayout = new VBox();
		mainLayout.getStyleClass().add("form-layout");

		Label label = UiUtils.buildTitle("Enter amount:", "input-title");
		input = new TextField();

		submit = new Button("submit");
		submit.setOnAction(this::onSubmit);

		error = UiUtils.buildTitle("You entered the wrong amount", "err");
		error.setVisible(false);

		mainLayout.getChildren().addAll(label, input, submit, error);
	}

	private void onSubmit(ActionEvent event) {
		Optional<Integer> value = validate();
		if (value.isPresent()) {
			consumer.accept(value.get());
			UiUtils.getStage(mainLayout).close();
		} else {
			error.setVisible(true);
		}
	}

	private Optional<Integer> validate() {
		Integer val = null;

		try {
			val = Integer.parseInt(input.getText());
		} catch (NumberFormatException ignored) {

		}

		return Optional.ofNullable(val);
	}

	@Override
	public void resize(double width, double height) {

	}

	@Override
	public Parent getView() {
		return mainLayout;
	}
}
