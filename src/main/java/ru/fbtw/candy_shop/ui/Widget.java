package ru.fbtw.candy_shop.ui;

import javafx.scene.Node;

public interface Widget {
	void resize(double width, double height);
	Node getView();
}
