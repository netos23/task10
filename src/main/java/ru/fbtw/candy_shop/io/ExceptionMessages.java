package ru.fbtw.candy_shop.io;

/**
 * Сообщения об ошибках при вводе - выводе
 */
public class ExceptionMessages {
	public static final String FILE_ERR = "Указан не верный путь или файл не удалось открыть";
	public static final String FILE_TYPE_ERR = "Указан не верный путь или файл не удалось открыть";
	public static final String INPUT_ERR = "Данные заданы в не верном формате";
	public static final String UNKNOWN = "Неизвестная ошибка";
	public static final String MISSING_FLAG = "Один или несколько флагов не были указаны";
}
