package ru.fbtw.candy_shop.io.cmd;

@FunctionalInterface
public interface ConsoleMethod {
	void execute(String ... args) throws Exception;
}
