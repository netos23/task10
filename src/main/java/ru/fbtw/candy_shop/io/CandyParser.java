package ru.fbtw.candy_shop.io;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import ru.fbtw.candy_shop.core.Candy;
import ru.fbtw.candy_shop.core.CandyPackage;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class CandyParser {
	private JsonObject root;
	private List<Candy> candies;
	private int balance;

	public CandyParser(String src) {
		root = JsonParser.parseString(src).getAsJsonObject();
		candies = new ArrayList<>();
		parse();
	}

	public CandyParser(File src) throws FileNotFoundException {
		Reader reader = new FileReader(src);
		root = JsonParser.parseReader(reader).getAsJsonObject();
		candies = new ArrayList<>();
		parse();
	}

	private void parse() {
		balance = root.get("balance").getAsInt();
		JsonArray candyJson = root.getAsJsonArray("candies");

		for (JsonElement element : candyJson) {
			parseCandy(element.getAsJsonObject());
		}
	}

	private void parseCandy(JsonObject json) {
		String name = json.get("name").getAsString();
		int price = json.get("price").getAsInt();
		Candy candy = new Candy(name, price);
		candies.add(candy);
	}

	public List<Candy> getCandies() {
		return candies;
	}

	public int getBalance() {
		return balance;
	}
}
