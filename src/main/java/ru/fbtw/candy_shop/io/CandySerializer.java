package ru.fbtw.candy_shop.io;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import ru.fbtw.candy_shop.core.Candy;
import ru.fbtw.candy_shop.core.CandyPackage;

import java.io.PrintStream;
import java.util.List;
import java.util.Map;

public class CandySerializer {
	private static final Gson gson = new Gson();

	public static void writeCart(PrintStream out, CandyPackage candyPackage) {
		JsonObject root = writeCart(candyPackage);
		writeJson(out, root);
	}

	public static void writeItems(PrintStream out, List<Candy> candies, int balance) {
		JsonObject root = writeShop(candies, balance);
		writeJson(out, root);
	}

	private static void writeJson(PrintStream out, JsonObject object) {
		String s = gson.toJson(object);
		out.println(s);
	}

	private static JsonObject writeCart(CandyPackage candyPackage) {
		JsonObject root = new JsonObject();
		root.addProperty("balance", candyPackage.getBalance());

		JsonArray array = new JsonArray();
		candyPackage.getCart()
				.entrySet()
				.stream()
				.map(CandySerializer::writePackagedCandy)
				.forEach(array::add);
		root.add("cart", array);
		return root;
	}

	private static JsonObject writePackagedCandy(Map.Entry<Candy, Integer> candy) {
		JsonObject object = new JsonObject();
		object.addProperty("count", candy.getValue());
		object.add("candy", writeCandy(candy.getKey()));
		return object;
	}


	private static JsonObject writeShop(List<Candy> candies, int balance) {
		JsonObject root = new JsonObject();

		root.addProperty("balance", balance);

		JsonArray array = new JsonArray();
		candies.stream()
				.map(CandySerializer::writeCandy)
				.forEach(array::add);

		root.add("candies", array);
		return root;
	}

	private static JsonElement writeCandy(Candy candy) {
		JsonObject object = new JsonObject();
		object.addProperty("name", candy.getName());
		object.addProperty("price", candy.getPrice());
		return object;
	}
}
