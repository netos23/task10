package ru.fbtw.candy_shop.core;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CandyConsumer {
	private int beginBalance;
	private List<Candy> shopItems;
	private boolean consumed;

	private Map<Candy, Integer> shoppingCart;
	private int endBalance;

	public CandyConsumer(int beginBalance, List<Candy> shopItems) {
		this.beginBalance = beginBalance;
		this.shopItems = shopItems;
	}

	public void consume() throws EmptyShopException, CandyConsumeException {
		if (consumed) {
			throw new CandyConsumeException();
		}
		endBalance = beginBalance;

		shoppingCart = new HashMap<>();
		int[] widths = getMaxCandyWeight();

		for (int i = 0; i < shopItems.size(); i++) {
			if (widths[i] != 0) {
				shoppingCart.put(shopItems.get(i), widths[i]);
			}
		}

		consumed = true;
	}

	/**
	 * Функция поиска оптимальных весов для каждого типа конфет
	 * 1) максимальный вес конфет достижим тогда и только тогда, когда цена за кг минимальна,
	 * для этого отсортируем конфеты в порядке возрастания весов и найдем максимальный вес
	 * 2) так как конфеты должны быть различны то найдем следующую кон
	 *
	 * @return - максимальный вес каждого типа конфет
	 */
	private int[] getMaxCandyWeight() throws EmptyShopException, CandyConsumeException {
		int n = shopItems.size();
		int[] candyCount = new int[n];
		Collections.sort(shopItems);

		if (n != 0) {
			int cheapestPrice = shopItems.get(0).getPrice();
			if (cheapestPrice == 0) {
				throw new CandyConsumeException();
			}
			candyCount[0] = beginBalance / cheapestPrice;
			endBalance = beginBalance % cheapestPrice;

			if (n > 1) {
				int nextDifferentPrice = shopItems.get(1).getPrice();

				while (endBalance < nextDifferentPrice) {
					endBalance += cheapestPrice;
					candyCount[0]--;
				}

				for (int i = 1; i < n; i++) {
					if (endBalance < shopItems.get(i).getPrice()) {
						candyCount[i - 1]++;
						endBalance -= shopItems.get(i - 1).getPrice();
						break;
					}

					if (i == n - 1) {
						candyCount[i]++;
						endBalance -= shopItems.get(i).getPrice();
					}
				}
			}
		} else {
			throw new EmptyShopException();
		}

		return candyCount;
	}

	public int getBeginBalance() {
		return beginBalance;
	}

	public List<Candy> getShopItems() {
		return shopItems;
	}

	public boolean isСonsumed() {
		return consumed;
	}

	public Map<Candy, Integer> getShoppingCart() throws CandyConsumeException, EmptyShopException {
		if (!consumed) {
			consume();
		}
		return shoppingCart;
	}

	public int getEndBalance() throws CandyConsumeException, EmptyShopException {
		if (!consumed) {
			consume();
		}
		return endBalance;
	}

	public CandyPackage toCandyPackage() throws EmptyShopException, CandyConsumeException {
		return new CandyPackage(getEndBalance(), getShoppingCart());
	}
}
