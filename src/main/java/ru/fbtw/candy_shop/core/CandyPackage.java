package ru.fbtw.candy_shop.core;

import java.util.Map;

public class CandyPackage {
	private int balance;
	private Map<Candy, Integer> cart;

	public CandyPackage(int balance, Map<Candy, Integer> cart) {
		this.balance = balance;
		this.cart = cart;
	}

	public int getBalance() {
		return balance;
	}

	public Map<Candy, Integer> getCart() {
		return cart;
	}
}
