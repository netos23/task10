package ru.fbtw.candy_shop.core;

public class Candy implements Comparable<Candy> {
	private String name;
	private int price;

	public Candy(String name, int price) {
		this.name = name;
		this.price = price;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	@Override
	public int compareTo(Candy o) {
		return Integer.compare(this.price, o.price);
	}
}
