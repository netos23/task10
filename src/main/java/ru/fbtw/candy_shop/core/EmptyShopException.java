package ru.fbtw.candy_shop.core;

public class EmptyShopException extends Exception {
	private static final String MESSAGE = "The shop is empty";

	public EmptyShopException() {
		super(MESSAGE);
	}
}
