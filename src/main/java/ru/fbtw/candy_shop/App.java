package ru.fbtw.candy_shop;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import ru.fbtw.candy_shop.core.*;
import ru.fbtw.candy_shop.io.CandyParser;
import ru.fbtw.candy_shop.io.CandySerializer;
import ru.fbtw.candy_shop.io.ExceptionMessages;
import ru.fbtw.candy_shop.io.FileType;
import ru.fbtw.candy_shop.io.cmd.ConsoleArgInputProcessor;
import ru.fbtw.candy_shop.ui.UiUtils;
import ru.fbtw.candy_shop.ui.candy.CandyItem;
import ru.fbtw.candy_shop.ui.candy.CandyModel;
import ru.fbtw.candy_shop.ui.screens.AddScreen;
import ru.fbtw.candy_shop.ui.screens.BalanceScreen;
import ru.fbtw.candy_shop.ui.screens.CartScreen;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.*;
import java.util.stream.Collectors;

public class App extends Application {
	private static final String TITLE = "SWEETSHOP";
	private static final String BALANCE_FORMAT = "Available balance: %d $";
	private static final int WIDTH = 410;
	private static final int HEIGHT = 230;
	private static final int V_GAP = 20;
	private static final int H_GAP = 20;
	private static final FileChooser.ExtensionFilter filter =
			new FileChooser.ExtensionFilter("JSON files (*.json)", "*.json");

	private static List<Candy> shopItems;
	private static int balance;

	private static PrintStream cartOut = System.out;
	private static PrintStream shopOut = System.out;
	private static File input;
	private static boolean isConsoleInput = true;
	private static boolean isConsoleOutput = true;
	private static boolean isHeadless = false;
	private static boolean closeOnFinish = true;

	private GridPane contentLayout;
	private Label balanceLabel;

	/**
	 * Метод переопределяет ввод с консоли на ввод с файла
	 *
	 * @param args - в параметре 0 содержиться путь к файлу который следует загрузить
	 * @throws FileNotFoundException  - не найден файл
	 * @throws InputMismatchException - ошибка чтения
	 */
	private static void setInput(String... args) throws FileNotFoundException, InputMismatchException {

		String filename = args[0];
		isConsoleInput = false;

		if (validateFileName(filename)) {

			input = new File(filename);

			if (!input.exists()) throw new FileNotFoundException(ExceptionMessages.FILE_ERR);

		} else {
			throw new InputMismatchException(ExceptionMessages.FILE_TYPE_ERR);
		}
	}

	/**
	 * @param fileName - имя файло которое следует проверить
	 * @return - возвращает true если фаил является текстовым
	 */
	private static boolean validateFileName(String fileName) {
		return fileName.endsWith(".json");
	}

	/**
	 * Метод переопределяет вывод в консоли на вывод в файл
	 *
	 * @param args - в параметре 0 содержиться путь к файлу в который следует сохранить
	 * @throws FileNotFoundException
	 * @throws InputMismatchException
	 */
	private static void setOutput(String... args) throws FileNotFoundException, InputMismatchException {
		try {
			String filename = args[0];
			isConsoleOutput = false;

			if (validateFileName(filename)) {
				String name = filename.substring(0, filename.length() - 6);
				File shop = new File(name + "-shop.json");
				File cart = new File(name + "-cart.json");

				cartOut = new PrintStream(cart);
				shopOut = new PrintStream(shop);

			} else {
				throw new InputMismatchException(ExceptionMessages.FILE_TYPE_ERR);
			}

		} catch (FileNotFoundException ex) {
			throw new FileNotFoundException(ExceptionMessages.FILE_ERR);
		}
	}

	/**
	 * Отключает оконный интерфейс
	 *
	 * @param args - аргументы отсутствуют
	 */
	private static void setIsHeadless(String... args) {
		isHeadless = true;
	}

	/**
	 * Просмотр помощи
	 *
	 * @param args - аргументы отсутствуют
	 */
	private static void getHelp(String... args) {
		String message = "Добро пожаловать в кондитерскую!\n" +
				"Программа позволяет покупать конфеты)\n" +
				"список команд: \n" +
				"--headless  позволяет отключить оконный интерфейс\n" +
				"--help запросить помощь\n" +
				"-i [dir] указать деррикторию из которой следует загрузить данные,\n" +
				"\tесли в headless режиме флаг не указан, то произойдет ошибка\n" +
				"-o [dir] указать деррикторию в которую следует сохранить данные,\n" +
				"\tесли в headless режиме флаг не указан, то произойдет ошибка\n";
		System.out.println(message);
		System.exit(0);
	}

	private static CandyPackage buyCandies() throws CandyConsumeException, EmptyShopException {
		CandyConsumer consumer = new CandyConsumer(balance, shopItems);
		consumer.consume();
		return consumer.toCandyPackage();
	}

	private static void loadContent() throws FileNotFoundException {
		CandyParser parser = new CandyParser(input);
		shopItems = parser.getCandies();
		balance = parser.getBalance();
	}

	private static void saveContent() throws EmptyShopException, CandyConsumeException {
		CandyPackage candyPackage = buyCandies();
		CandySerializer.writeCart(cartOut, candyPackage);
		CandySerializer.writeItems(shopOut, shopItems, balance);
	}

	public static void main(String... args) {
		ConsoleArgInputProcessor processor = new ConsoleArgInputProcessor();

		processor.setFlagEvent("--help", App::getHelp)
				.setFlagEvent("--headless", App::setIsHeadless)
				.setFlagEvent("-i", App::setInput)
				.setFlagEvent("-o", App::setOutput)
				.setFlagEvent("--d", App::setDaemon);

		try {
			processor.execute(args);
		} catch (Exception e) {
			System.err.println(e.getMessage());
			System.exit(-1);
		}

		if (!isConsoleInput) {
			try {
				loadContent();
			} catch (FileNotFoundException e) {
				System.err.println(e.getMessage());
				System.exit(-1);
			}
		} else {
			shopItems = new ArrayList<>();
		}

		if (isHeadless) {
			if (isConsoleInput || isConsoleOutput) {
				System.err.println(ExceptionMessages.MISSING_FLAG);
				System.exit(-1);
			}

			try {
				saveContent();
				if (closeOnFinish) {
					System.exit(0);
				}
			} catch (EmptyShopException | CandyConsumeException e) {
				System.err.println(e.getMessage());
				if(closeOnFinish) {
					System.exit(-1);
				}
			}
		} else {
			launch(args);
		}
	}

	private static void setDaemon(String... strings) {
		closeOnFinish = false;
	}

	private List<Node> shopItemsToModel() {
		return shopItems.stream()
				.map(CandyItem::new)
				.map(CandyItem::getView)
				.collect(Collectors.toList());
	}

	private final ChangeListener<Number> resize = (observable, oldValue, newValue) -> {
		int columnCount = (int) Math.floor((double) newValue / (CandyItem.WIDTH + H_GAP));

		ObservableList<Node> children = contentLayout.getChildren();
		ObservableList<Node> values = FXCollections.observableArrayList(children);
		if (values.size() != 0) {
			UiUtils.getStage(contentLayout)
					.setMinHeight(CandyItem.HEIGHT + HEIGHT);
		} else {
			UiUtils.getStage(contentLayout)
					.setMinHeight(HEIGHT);
		}

		children.clear();
		for (int i = 0, row = 0; i < values.size(); ++i, row = i / columnCount) {
			contentLayout.add(values.get(i), i % columnCount, row);
		}

		contentLayout.setMinWidth((double) newValue);

	};

	@Override
	public void start(Stage primaryStage) throws Exception {
		primaryStage.widthProperty()
				.addListener(resize);
		primaryStage.setMinWidth(WIDTH);
		primaryStage.setMinHeight(HEIGHT);


		VBox mainLayout = new VBox();
		mainLayout.getStyleClass().add("layout");

		Label title = UiUtils.buildTitle(App.TITLE, "title");

		contentLayout = new GridPane();
		contentLayout.setAlignment(Pos.CENTER);
		contentLayout.setPadding(new Insets(20));
		updateBody();
		contentLayout.setHgap(H_GAP);
		contentLayout.setVgap(V_GAP);

		HBox buttonsPanel = buildButtons();

		updateBalance();

		mainLayout.getChildren().addAll(title, contentLayout, buttonsPanel, balanceLabel);

		Scene scene = new Scene(new ScrollPane(mainLayout), 800, 600);
		scene.getStylesheets().add("main.css");
		primaryStage.setScene(scene);
		primaryStage.setTitle(TITLE);
		primaryStage.show();
	}

	private void updateBody() {
		ObservableList<Node> children = contentLayout.getChildren();
		children.clear();
		children.addAll(shopItemsToModel());
	}

	private void updateBalance() {
		if (balanceLabel == null) {
			balanceLabel = UiUtils.buildTitle("", "price");
			balanceLabel.setOnMouseClicked(this::setBalance);
		}

		String balanceText = String.format(BALANCE_FORMAT, balance);
		balanceLabel.setText(balanceText);

	}

	private HBox buildButtons() {
		HBox layout = new HBox();
		layout.getStyleClass().add("buttons-bar");

		Button buy = new Button("Buy");
		buy.setOnAction(this::showCartScreen);

		Button save = new Button("Save");
		save.setOnAction(this::showSaveDialog);

		Button load = new Button("Load");
		load.setOnAction(this::showLoadDialog);

		Button add = new Button("Add");
		add.setOnAction(this::showAddScreen);

		layout.getChildren().addAll(buy, save, load, add);
		return layout;
	}

	private void showCartScreen(ActionEvent event) {
		try {
			CandyPackage candyPackage = buyCandies();
			Map<Candy, CandyModel> map = candyPackage.getCart()
					.keySet()
					.stream()
					.collect(Collectors.toMap(v -> v, v -> CandyModel.getRandomModel(50)));
			CartScreen cartScreen = new CartScreen(candyPackage, map);
			showNewStage(cartScreen.getView(), "Cart", false);
		} catch (CandyConsumeException | EmptyShopException e) {
			e.printStackTrace();
		}

	}

	private void addItem(CandyItem item) {
		shopItems.add(item.getCandy());
		contentLayout.getChildren().add(item.getView());
		double width = UiUtils.getStage(contentLayout).getWidth();
		resize.changed(null, 0, width);
	}

	private void showSaveDialog(ActionEvent event) {
		try {
			File file = openFileDialog(FileType.SAVE);
			Objects.requireNonNull(file);
			setOutput(file.getAbsolutePath());
			saveContent();
		} catch (FileNotFoundException | NullPointerException e) {
			System.err.println(ExceptionMessages.FILE_ERR);
		} catch (CandyConsumeException | EmptyShopException e) {
			System.err.println(ExceptionMessages.UNKNOWN);
		}
	}

	private void showLoadDialog(ActionEvent event) {
		try {
			File file = openFileDialog(FileType.LOAD);
			Objects.requireNonNull(file);

			setInput(file.getAbsolutePath());
			loadContent();


			updateBalance();
			updateBody();

			double width = UiUtils.getStage(contentLayout).getWidth();
			resize.changed(null, 0, width);
		} catch (FileNotFoundException | NullPointerException e) {
			System.err.println(ExceptionMessages.FILE_ERR);
		}
	}

	private File openFileDialog(FileType fileType) {
		FileChooser fileChooser = new FileChooser();
		fileChooser.getExtensionFilters().add(filter);
		Stage stage = UiUtils.getStage(contentLayout);

		return fileType == FileType.LOAD
				? fileChooser.showOpenDialog(stage)
				: fileChooser.showSaveDialog(stage);
	}

	private void showAddScreen(ActionEvent event) {
		AddScreen addScreen = new AddScreen();
		addScreen.setItemConsumer(this::addItem);
		showNewStage(addScreen.getView(), "Add", false);
	}

	private void setBalance(MouseEvent event) {
		BalanceScreen balanceScreen = new BalanceScreen(
				v -> {
					balance = v;
					updateBalance();
				},
				balance
		);

		showNewStage(balanceScreen.getView(), "Balance", false);

	}


	private void showNewStage(Parent mainLayout, String title, boolean resize) {
		Scene scene = new Scene(mainLayout);
		scene.getStylesheets().add("main.css");

		Stage stage = new Stage(StageStyle.UTILITY);
		stage.setScene(scene);
		stage.initModality(Modality.APPLICATION_MODAL);
		stage.setResizable(resize);
		stage.setTitle(title);
		stage.show();
	}
}
