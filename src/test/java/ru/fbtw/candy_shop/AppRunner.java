package ru.fbtw.candy_shop;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.File;
import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Stream;

public class AppRunner {

	private static final String OUTPUT_PATH = "test_output";
	private static final String TEST_PATH = "tests";

	@BeforeAll
	static void setup() {
		File file = new File(OUTPUT_PATH);
		if (file.exists()) {
			file.delete();
			file.mkdir();
		} else {
			file.mkdir();
		}
	}

	private static Stream<File> fileProvider() {
		File file = new File(TEST_PATH);
		return Arrays.stream(Objects.requireNonNull(file.listFiles()));
	}

	@ParameterizedTest
	@MethodSource("fileProvider")
	void run(File test) {
		String output = setupTestFolder(test.getName());
		App.main("--headless", "-i", test.getAbsolutePath(), "-o", output,"--d");
	}
	@Test
	void wrongParams(){
		App.main("--headless");
	}

	String setupTestFolder(String name) {
		String dirname = name.substring(0, name.length() - 5);
		File testOutput = new File(OUTPUT_PATH + "/" + dirname);
		testOutput.mkdir();
		return testOutput.getAbsolutePath() + "\\" + name;
	}


}
